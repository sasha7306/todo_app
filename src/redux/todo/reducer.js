import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  todoList: [],
  taskDeleted: null,
  taskAdded: null,
});

export default function todoReducer(state = initState, action) {
  switch (action.type) {
    case actions.GET_ALL_SUCCESS:
      return state.set("todoList", action.payload);
    case actions.GET_TODAY_SUCCESS:
      return state.set("todoList", action.payload);
    case actions.DELETE_TASK_SUCCESS:
      return state.set("taskDeleted", action.payload);
    case actions.ADD_TASK_SUCCESS:
      return state.set("taskAdded", action.payload);
    default: return state;
  }
}
