const todoAction = {
  GET_ALL: 'GET_ALL',
  GET_ALL_SUCCESS: 'GET_ALL_SUCCESS',
  GET_TODAY: 'GET_TODAY',
  GET_TODAY_SUCCESS: 'GET_TODAY_SUCCESS',
  DELETE_TASK: 'DELETE_TASK',
  DELETE_TASK_SUCCESS: 'DELETE_TASK_SUCCESS',
  ADD_TASK: 'ADD_TASK',
  ADD_TASK_SUCCESS: 'ADD_TASK_SUCCESS',
  CHECK_TASK: 'CHECK_TASK',

  getAll: () => ({
    type: todoAction.GET_ALL,
  }),

  getAllSuccess: data => ({
    type: todoAction.GET_ALL_SUCCESS,
    payload: data,
  }),

  getToday: () => ({
    type: todoAction.GET_TODAY,
  }),

  getTodaySuccess: data => ({
    type: todoAction.GET_TODAY_SUCCESS,
    payload: data,
  }),

  deleteTask: data => ({
    type: todoAction.DELETE_TASK,
    payload: data,
  }),

  deleteTaskSuccess: data => ({
    type: todoAction.DELETE_TASK_SUCCESS,
    payload: data,
  }),

  addTask: data => ({
    type: todoAction.ADD_TASK,
    payload: data,
  }),

  addTaskSuccess: data => ({
    type: todoAction.ADD_TASK_SUCCESS,
    payload: data,
  }),

  checkTask: data => ({
    type: todoAction.CHECK_TASK,
    payload: data,
  }),
};
export default todoAction;
