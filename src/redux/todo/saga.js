import {put, takeEvery} from 'redux-saga/effects';
import actions from './actions';

export function* getAllList() {
  let listData = localStorage.getItem('todoList');

  if(!listData) {
    listData = [];
    //set fake data
    for (let i = 0; i < 7; i++) {
      listData.push({
        id: Date.now()%10000 + i,
        active: !(i%2),
        date: new Date(Date.now() + i* 1000*60*60*8),
        todo: `${i} some hard task, very important not forget`,
      });
    }
    localStorage.setItem('todoList', JSON.stringify(listData));
  }else{
    listData = JSON.parse(listData)
  }

  yield put(actions.getAllSuccess(listData));
}

export function* getTodayList() {
  let listData = JSON.parse(localStorage.getItem('todoList'));

  const getDays = (ms) => Math.floor(ms/(1000*60*60*24));
  listData = listData.filter(x=> getDays(new Date(x.date).valueOf()) === getDays(Date.now()));

  yield put(actions.getTodaySuccess(listData));
}

export function* addTask({payload}) {
  let listData = JSON.parse(localStorage.getItem('todoList'));
  payload.id = Date.now()%10000;
  payload.active = false;

  listData.push(payload);
  localStorage.setItem('todoList', JSON.stringify(listData));

  yield put(actions.addTaskSuccess(payload));
}

export function* deleteTask({payload}) {
  let listData = JSON.parse(localStorage.getItem('todoList'));

  listData.splice(listData.findIndex(x=>x.id===payload),1);
  localStorage.setItem('todoList', JSON.stringify(listData));

  yield put(actions.deleteTaskSuccess(payload));
}

export function checkTask({payload}) {
  const {id, active} = payload;
  let listData = JSON.parse(localStorage.getItem('todoList'));

  const item = listData.find(x=>x.id===id);
  item.active = active;

  localStorage.setItem('todoList', JSON.stringify(listData));
}

export default function* rootSaga() {
  yield takeEvery(actions.GET_ALL, getAllList);
  yield takeEvery(actions.GET_TODAY, getTodayList);
  yield takeEvery(actions.ADD_TASK, addTask);
  yield takeEvery(actions.DELETE_TASK, deleteTask);
  yield takeEvery(actions.CHECK_TASK, checkTask);
}
