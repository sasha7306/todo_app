import './App.css';
import todoAction from "./redux/todo/actions";
import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";
import { List, Menu, Modal, Icon, Layout, Checkbox, Popconfirm, message, Input, DatePicker } from 'antd';

const { Header, Content, Footer } = Layout;
const { getAll, getToday, addTask, deleteTask, checkTask } = todoAction;

class App extends Component {
  constructor(props) {
    super(props);

    props.getAll();
    this.state = {
      typeList: 'all',
      listData: [],
      showModal: false,
      newTask: '',
      newTaskDate: null
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.taskDeleted && nextProps.taskDeleted!==this.props.taskDeleted){
      message.success('task deleted');
      this.updateList();
    }

    if(nextProps.taskAdded && (!this.props.taskAdded || nextProps.taskAdded.id!==this.props.taskAdded.id)){
      message.success('task added');
      this.updateList();
    }

    this.setState({listData: nextProps.todoList})
  }

  updateList = () => {
    switch (this.state.typeList) {
      case 'all':
        this.props.getAll();
        break;
      case 'today':
        this.props.getToday();
        break;
      default:
    }
  };

  onCheckTodo = (target, id) => {
    const {listData} = this.state;
    const item = listData.find(x=>x.id===id);
    item.active = target.checked;

    this.props.checkTask({
      id,
      active: target.checked
    });
    this.setState(listData);
  };

  onChangePage = (page) => {
  };

  confirmDelete = (id) => {
    this.props.deleteTask(id);
  };

  //menu function
  onMenuClick = ({ item, key, keyPath }) =>{
    switch (key) {
      case "new":
        this.addNewTask();
        break;
      case "all":
        this.loadAllTasks();
        break;
      case "today":
        this.loadTodayTasks();
        break;
      default:
    }
  };

  addNewTask = () => {
    this.setState({showModal: true});
  };

  loadAllTasks = () => {
    this.setState({typeList: 'all'});
    this.props.getAll();
  };

  loadTodayTasks = () => {
    this.setState({typeList: 'today'});
    this.props.getToday();
  };
  //end menu function

  //modal functions
  handleOkModal = (e) => {
    const {newTaskDate, newTask} = this.state;
    this.props.addTask({
      date: newTaskDate,
      todo: newTask,
    });

    this.closeModal();
  };

  handleCancelModal = (e) => {
    this.closeModal();
  };

  closeModal = () => {
    this.setState({
      showModal: false,
      newTaskDate: null,
      newTask: ''
    });
  };

  onDateChange = (date, dateString) => {
    this.setState({newTaskDate: date});
  };

  onTaskChange = (e) => {
    this.setState({newTask: e.target.value});
  };
  //end modal functions

  //render task item
  renderItem = (item) => {
    return(<List.Item
      key={item.id}
      actions={[<span>{new Date(item.date).toDateString()}</span>]}
      extra={
        <Popconfirm title="Are you sure delete this task?" onConfirm={()=>this.confirmDelete(item.id)} okText="Yes" cancelText="No">
         <Icon type="delete" />
        </Popconfirm>
      }
    >
      <Checkbox checked={item.active} onChange={(e)=>this.onCheckTodo(e.target, item.id)}>{item.todo}</Checkbox>
    </List.Item>)
  };

  render() {
    const {listData, showModal, newTask, newTaskDate} = this.state;

    return (
      <div className="App">
        <Modal
          title="Add new Task"
          visible={showModal}
          onOk={this.handleOkModal}
          onCancel={this.handleCancelModal}
        >
          <p><Input value={newTask} onChange={this.onTaskChange} placeholder="input task" /></p>
          <DatePicker value={newTaskDate} onChange={this.onDateChange} placeholder="task deadline"  />
        </Modal>
        <Layout>
          <Header style={{  width: '100%' }}>
            <Menu
              theme="dark"
              mode="horizontal"
              selectable = {false}
              style={{ lineHeight: '64px' }}
              onClick={this.onMenuClick}
            >
              <Menu.Item key="all">All tasks</Menu.Item>
              <Menu.Item key="today">Today tasks</Menu.Item>
              <Menu.Item key="new">Add new</Menu.Item>
            </Menu>
          </Header>
          <Content style={{ padding: '0 50px', marginTop: 64 }}>
            <div style={{ background: '#fff', padding: 24, minHeight: 700 }}>
              <List
                itemLayout="vertical"
                size="large"
                pagination={{
                  onChange: this.onChangePage,
                  pageSize: 7,
                }}
                dataSource={listData}
                renderItem={this.renderItem.bind(this)}
              />
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            TODO ©2019 Created by Alex
          </Footer>
        </Layout>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { todoList, taskDeleted, taskAdded } = state.Todo.toJS();
  return { todoList, taskDeleted, taskAdded };
}

function mapDispatchToProps(dispatch) {
  return {
    getAll: () => dispatch(getAll()),
    getToday: () => dispatch(getToday()),
    addTask: (data) => dispatch(addTask(data)),
    deleteTask: (id) => dispatch(deleteTask(id)),
    checkTask: (data) => dispatch(checkTask(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
